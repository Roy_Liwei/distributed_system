package first_servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jdk.nashorn.internal.ir.RuntimeNode.Request;

/**
 * Servlet implementation class f_servlet
 */
@WebServlet("/f_servlet")
public class f_servlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static ConcurrentHashMap<String,String> hm = new ConcurrentHashMap<> ();
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public f_servlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Cookie[] cookies = request.getCookies();        		// get client's cookie array		
		String act1 = request.getParameter("Replace");  		// act1 = replace
		String act2 = request.getParameter("Refresh");  		// act2 = refresh
		String act3 = request.getParameter("Log_out");  		// act3 = logout
		Cookie c_cookie = null;                         		// initialize c_cookie
		
		PrintWriter out = response.getWriter();         		// response  
		
		response.setContentType("text/html");
		
		boolean find_cookie = false;                    		// flag, whether client's request contain the cookie
		
																	
		try {
			remove_oldsess();
		} catch (ParseException e) {							// scan the hashmap and remove stale sessions
			
			e.printStackTrace();
		}
		
			
		if ((act1 == null && act2 == null && act3 == null) || cookies == null ) {         // no action, print "welcome page" 
			Session new_session = new Session("Hello User!");
																        // cookie's value
			String v = "" + new_session.get_sid() + "_" 
					+ new_session.get_vnum() 
					+ "_" + new_session.get_data();
				              										     // new cookie
			c_cookie = new Cookie("CS5300PROJ1SESSION", v);
				
			c_cookie.setMaxAge(30);
				                                                         //session's info 
			String sess_info = new_session.get_vnum() + "," 
							+ new_session.get_mess() + "," 
							+ new_session.get_time();
				                                                         //put session info into hashmap
			hm.put(new_session.get_sid().toString(), sess_info);
				                                                         // response print
			out.println(output(new_session.get_time(), 
						new_session.get_mess(), 
						new_session.get_sid() + "", 
						new_session.get_vnum(), v));
				
			response.addCookie(c_cookie);                            // send cookie back
				 
			}
			
			if (cookies != null)  {									// if cookies != null
				
				for (int i = 0; i < cookies.length; i++) {
					if (cookies[i].getName().equals("CS5300PROJ1SESSION")) {
					
						find_cookie = true;							// find if client's request have this cookie, if it has,
																	// set flag find_cookie = true and give this cookie
						c_cookie = cookies[i];						// to c_cookie		 
																	
						c_cookie.setMaxAge(30);							  
																	
					break;											
					}
				}

		    
			if (act1 != null) {											 // action = replace, find if it has the cookie.
				
				if (!find_cookie) {										 // doesn't have the cookie,(session expire) 
					Session new_session = new Session("Hello User!");	 // build new session and new cookie
					
					String v = "" + new_session.get_sid() + "_" 		 // cookie value
							+ new_session.get_vnum() + "_" 
							+ new_session.get_data();
					
					c_cookie = new Cookie("CS5300PROJ1SESSION", v);      // new cookie
					c_cookie.setMaxAge(30);
					
					String sess_info = new_session.get_vnum() + "," 
									+ new_session.get_mess() + "," 		 //session info
									+ new_session.get_time();
					
					hm.put(new_session.get_sid().toString(), sess_info); //put session into hashmap
					
					out.println(output(new_session.get_time(), 
								new_session.get_mess(), 
								new_session.get_sid() + "",              // response message
								new_session.get_vnum(), v));              
					
					response.addCookie(c_cookie);                 		// send cookie back 
				}
				
				/*
				 *  if it has the cookie, get corresponding session out of the hashmap, get its info,
				 *  update its version number, expire time and message, and put it back
				 */
				else {
					String[] c_info = c_cookie.getValue().split("_");    
																		 
					String s = (String) hm.get(c_info[0]);              
					 													 
																		 
					String[] s_info = s.split(",");
					
					int v_num = Integer.parseInt(s_info[0]) + 1;          // update version number
					
					DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date dt = new Date(System.currentTimeMillis()+30*1000);
					s_info[2] = dateFormat.format(dt);                         // update expire time
					s_info[0] = "" + v_num;
					s_info[1] = request.getParameter("message");        
					c_info[1] = "" + v_num;
					
					String new_s = s_info[0] + "," + s_info[1] + "," + s_info[2];
					
					String new_c = c_info[0] + "_" + c_info[1] + "_" + c_info[2] 
									+ "_" + c_info[3];
					
					hm.put(c_info[0], new_s);
					
					out.println(output(s_info[2], s_info[1], c_info[0], 
						Integer.parseInt(s_info[0]), new_c));
					
					response.addCookie(c_cookie);
				}
			}
			
			else if (act2 != null) {  										// action == refresh,find if it has the cookie.
				
				if (!find_cookie) {                                         // doesn't have, new session and new cookie
					Session new_session = new Session("Hello User!");
					
					String v = "" + new_session.get_sid() + "_" 
							 + new_session.get_vnum()+ "_" 
							 + new_session.get_data();
					
					c_cookie = new Cookie("CS5300PROJ1SESSION", v);
					c_cookie.setMaxAge(30);
					
					String sess_info = new_session.get_vnum() + "," 
									+ new_session.get_mess() + "," 
									+ new_session.get_time();
					
					hm.put(new_session.get_sid().toString(), sess_info);
					
					out.println(output(new_session.get_time(), 
								new_session.get_mess(), 
								new_session.get_sid() + "", 
								new_session.get_vnum(), v));
					response.addCookie(c_cookie);	
				}
				
				/*
				 * has the cookie, get the session out of the hashmap, 
				 * get its info and update the version number and expire time.
				 */
				
				else {														
					           							
					String[] c_info = c_cookie.getValue().split("_");       // get session ID
					
					String s = (String) hm.get(c_info[0]);                  // get session info
					 
					
				    String[] s_info = s.split(",");                         // get version number
					
				    int v_num = Integer.parseInt(s_info[0]) + 1;            // update version number
				   
				    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");   // update expire time
					Date dt = new Date(System.currentTimeMillis()+30*1000);
					s_info[2] = dateFormat.format(dt);
					s_info[0] = "" + v_num;
					c_info[1] = "" + v_num;
					
					String new_s = s_info[0] + "," + s_info[1] + "," + s_info[2];
				
					String new_c = c_info[0] + "_" + c_info[1] + "_" + c_info[2] + "_" + c_info[3];
					
					hm.put(c_info[0], new_s);
					
					out.println(output(s_info[2], s_info[1], c_info[0], 
							Integer.parseInt(s_info[0]), new_c));
					
					response.addCookie(c_cookie);
					
					
				}	
			}
			
			/*
			 * action == logout, (if it has the cookie, set its maxage to zero), redirect it to a different page.
			 */
			else if (act3 != null) {       
				if (find_cookie) {
					String[] c_info = c_cookie.getValue().split("_");
					hm.remove(c_info[0]);
					c_cookie.setMaxAge(0);
					response.addCookie(c_cookie);
				}
				out.println("<h1>successfully log out</h1>");
			}
		}
	}
	
	
	
	// response message print method
	public String output(String time, String mess, String sid, int vnum, String cookie) 
			throws ServletException, IOException{

		    String opt = "<!DOCTYPE html>\n" +
						 "<html>\n" +
						 "<head>\n" +
						 "<title>Insert title here</title>\n"
						 + "</head>\n" 
						 +"<body>\n" 
						 + "<h1>" + mess+ "</h1>" 
						 + "<form metod = \"get\">" 
						 + "<p>Netid: lh567 </p>\n" + "\n"
						 + "<p>Expire:" + time + "</p>\n" + "\n"
						 + "<p>Session:" + sid + "</p>\n" + "\n"
						 + "<p>Version:" + vnum + "</p>\n" + "\n"
						 + "<p>Cookie:" + cookie + "</p>\n" + "\n"
						 + "<div><input type = \"submit\" name = \"Refresh\" value = \"Refresh\" ></div>"
						 + "<div><input type = \"submit\" name = \"Replace\" value = \"Replace\" ><input type"
						 + "= \"text\" name = \"message\"></div>"
						 + "<div><input type = \"submit\" name = \"Log_out\" value = \"Logout\"></div>"
						 + "</form>"
						 + "</body>"
						 + "</html>"; 
			return opt; 
		}	
	
	
	// remove stale sessions method
	public void remove_oldsess() throws ParseException {
		Iterator iter = hm.entrySet().iterator();
	    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date c_time = new Date(System.currentTimeMillis());
		
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next(); 
			Object key = entry.getKey();
			String val = (String)entry.getValue(); 
			String[] s_info = val.split(",");
			Date dt = dateFormat.parse(s_info[2]);
			if (dt.before(c_time)) {
				hm.remove(key);
			}
		}
	}


}


