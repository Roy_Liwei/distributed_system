package first_servlet;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/*
 * session class, contain 5 fields (version number, expire time, session ID, message, and location_data) 
 * and corresponding 5 getter method.
 * constructor initialize the 5 fields' value.
 */
public class Session {
	private int version_number;
	private String expire_time;
	private UUID session_id;
	private String message;
	private String metadata = "0_0";
	public Session(String s) {
		session_id = UUID.randomUUID();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dt = new Date(System.currentTimeMillis()+30*1000);
		expire_time = dateFormat.format(dt);
		version_number = 1;
		message = s;
	}
	public String get_time() {
		return expire_time;
	}
	public UUID get_sid() {
		return session_id;
	}
	public String get_mess() {
		return message;
	}
	public int get_vnum() {
		return version_number;
	}
	public String get_data() {
		return metadata;
	}
}
